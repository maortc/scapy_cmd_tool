from scapy.all import *

DNS_SERVER_IP = "8.8.8.8"
PORT = 53
MIN_ANSWER_LEN = 8


def nslookup(domain):
    """The function is looking for the ip of the given domain
    :param domain: the domain
    :type domain: str
    :return: the ip
    :rtype: str
    """
    ans = sr1(IP(dst=DNS_SERVER_IP)/UDP(dport=PORT)/DNS(rd=1, qd=DNSQR(qname=domain)), verbose=0)[DNS].an.rdata
    return ans if not False else f"can't find {domain}: Non-existent domain"


def main():
    domain = input("Please enter domain: ")
    print(nslookup(domain))


if __name__ == "__main__":
    main()

