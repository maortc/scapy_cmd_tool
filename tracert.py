from scapy.all import *
import socket
import nslookup

DNS_SERVER_IP = "8.8.8.8"
PORT = 53
MIN_ANSWER_LEN = 8

MAX = 20


def tracert(domain):
    """The function is making a tracert to the given domain
    :param domain: the domain
    :type domain: str
    :return: None
    """
    ttl_req = 1
    ans_ip = 'none'
    pre_ans = 'a'
    stop = False
    ip_domain = nslookup.nslookup(domain)
    if "Non-existent domain" in ip_domain and domain.isalpha():
        print(f"Unable to resolve target system name {domain}.")
    else:
        while not ttl_req == MAX and not stop and pre_ans not in ans_ip:
            if not ans_ip == "none":
                pre_ans = ans_ip
            ans = srp1(Ether() / IP(dst=domain, ttl=ttl_req) / ICMP(), verbose=0, timeout=1)
            if ans:
                ans_ip = ans[IP].src
                print("%d: %s : " % (ttl_req, ans_ip), end='')
                try:
                    ans_ip = socket.gethostbyaddr(ans_ip)[0]
                except socket.herror:
                    ans_ip = 'none'
                print(ans_ip)
            else:
                stop = True
            ttl_req += 1


def main():
    domain = input("Please enter domain or ip: ")
    tracert(domain)


if __name__ == "__main__":
    main()
