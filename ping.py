from scapy.all import *
import datetime
import nslookup

MICRO_MIL_SEC = 0.001
TIMES = 3

DNS_SERVER_IP = "8.8.8.8"
PORT = 53
MIN_ANSWER_LEN = 8


def ping_once(domain):
    """The function is pinging once to the given domain and returning the time
    :param domain: the domain
    :type domain: str
    :return: the time
    :rtype: int
    """

    time_ans = datetime.datetime.now()
    ans = srp1(Ether() / IP(dst=domain) / ICMP(), verbose=0)
    time_ans = datetime.datetime.now() - time_ans
    return int(time_ans.microseconds * MICRO_MIL_SEC)


def ping(domain):
    """The function is making a ping requests to the given domain
    :param domain: the domain
    :type domain: str
    :return: None
    """
    avg = 0
    if "Non-existent domain" in nslookup.nslookup(domain):
        print(f"Ping request could not find host {domain}. Please check the name and try again.")
    else:
        print(f"Pinging {domain}")
        for i in range(TIMES):
            print(f"Reply from {domain}: ", end='')
            time_ping = ping_once(domain)
            avg += time_ping
            print(f"time={time_ping}ms")
            time.sleep(0.25)
        print(f"Average = {int(avg/TIMES)}ms")


def main():
    domain = input("Please enter domain: ")
    ping(domain)


if __name__ == "__main__":
    main()

